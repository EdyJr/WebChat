package QueueCreator;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;

@WebService
public interface QueueCreatorItf {

    @WebMethod void createQueue(String name);
    @WebMethod void addOfflineMessages(String sender, String receiver, String content);
    @WebMethod ArrayList<String> getOfflineMessages(String name);
    @WebMethod void removeQueue(String name);
}