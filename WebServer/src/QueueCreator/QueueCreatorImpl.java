package QueueCreator;

import org.exolab.jms.administration.AdminConnectionFactory;
import org.exolab.jms.administration.JmsAdminServerIfc;
import javax.jms.*;
import javax.jws.WebService;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.ArrayList;
import java.util.Hashtable;

@WebService(endpointInterface = "QueueCreator.QueueCreatorItf")
public class QueueCreatorImpl implements QueueCreatorItf {

    private Context context;

    private Session session;

    private Connection connection;

    private JmsAdminServerIfc admin;

    @Override
    public void createQueue(String name) {

        String url = "tcp://localhost:3035/";
        Hashtable properties = new Hashtable();
        properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.exolab.jms.jndi.InitialContextFactory");
        properties.put(Context.PROVIDER_URL, url);
        try {
            admin = AdminConnectionFactory.create(url);
            String queue = name;
            if (!admin.destinationExists(queue)) {
                Boolean isQueue = Boolean.TRUE;
                if (!admin.addDestination(queue, isQueue)) {
                    System.err.println("Failed to create queue " + queue);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            context = new InitialContext(properties);
            ConnectionFactory factory = (ConnectionFactory) context.lookup("ConnectionFactory");
            connection = factory.createConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            connection.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addOfflineMessages(String sender, String receiver, String content) {

        String msgTemp = "sender: " + sender + "content: " + content;
        try {
            Destination destination = (Destination) context.lookup(receiver);
            MessageProducer mSender = session.createProducer(destination);
            TextMessage message = session.createTextMessage(msgTemp);
            mSender.send(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<String> getOfflineMessages(String name) {

        ArrayList<String> msgs = new ArrayList<>();
        MessageConsumer receiver;
        try {
            Destination destination = (Destination) context.lookup(name);
            receiver = session.createConsumer(destination);
        } catch (Exception e) {
            e.printStackTrace();
            return msgs;
        }
        while (true) {
            try {
                TextMessage textMessage = (TextMessage) receiver.receiveNoWait();
                if (textMessage == null)
                    return msgs;
                String msg = textMessage.getText();
                String senderStr = "sender: ";
                String contentStr = "content: ";
                String sender = msg.substring(senderStr.length(), msg.indexOf(contentStr));
                String content = msg.substring(msg.indexOf(contentStr) + contentStr.length());
                String msgTemp = "sender: " + sender + "content: " + content;
                msgs.add(msgTemp);
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void removeQueue(String name) {

        try {
            if (!admin.removeDestination(name)) {
                System.err.println("Failed to remove destination " + name);
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}