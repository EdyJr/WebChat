import QueueCreator.QueueCreatorImpl;

import javax.xml.ws.Endpoint;

public class WebPublisher {

    public static void main(String[] args) {

        String adress = "http://" + args[0] + ":" + args[1] + "/queueCreator";
        Endpoint.publish(adress, new QueueCreatorImpl());
    }
}