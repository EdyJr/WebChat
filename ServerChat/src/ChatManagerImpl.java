import ChatModule.ChatManagerPOA;
import ChatModule.NonexistentContact;
import ClientModule.ClientManager;
import ClientModule.ClientManagerHelper;
import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CORBA.Object;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import queuecreator.QueueCreatorImplService;
import queuecreator.QueueCreatorItf;

public class ChatManagerImpl extends ChatManagerPOA {

    NamingContext clientContext;

    public ChatManagerImpl(String args[]) {

        ORB orb = ORB.init(args, null);
        Object obj = null;
        try {
            obj = orb.resolve_initial_references("NameService");
        } catch (InvalidName invalidName) {
            invalidName.printStackTrace();
        }
        clientContext = NamingContextHelper.narrow(obj);
    }

    @Override
    public boolean getIsOnline(String contactName) throws NonexistentContact {

        Object objRef = getObjectRef(contactName);
        ClientManager client = ClientManagerHelper.narrow(objRef);
        return client.getIsOnline();
    }

    @Override
    public boolean writeMessage(String sender, String receiver, String message) throws NonexistentContact {

        Object objRef = getObjectRef(receiver);
        ClientManager client = ClientManagerHelper.narrow(objRef);
        boolean isContactOnline = client.getIsOnline();
        if (isContactOnline) {
            client.writeToChat(sender, message);
            return true;
        }
        QueueCreatorImplService service = new QueueCreatorImplService();
        QueueCreatorItf creatorService = service.getQueueCreatorImplPort();
        creatorService.addOfflineMessages(sender, receiver, message);
        return false;
    }

    @Override
    public void establishFriendship(String friend1, String friend2) throws NonexistentContact {

        Object objRef = getObjectRef(friend1);
        ClientManager client = ClientManagerHelper.narrow(objRef);
        client.addNewContact(friend2);
    }

    @Override
    public void updateStatus(String listener, String contact, boolean isOnline) throws NonexistentContact {

        Object objRef = getObjectRef(listener);
        ClientManager client = ClientManagerHelper.narrow(objRef);
        client.updateStatus(contact, isOnline);
    }

    public Object getObjectRef(String nameRef) throws NonexistentContact {

        NameComponent[] name = { new NameComponent("ClientManager: " + nameRef,"") };
        Object objRef = null;
        try {
            objRef = clientContext.resolve(name);
        } catch (NotFound notFound) {
            System.out.println(nameRef + " Not Found");
            throw new NonexistentContact(nameRef);
        } catch (CannotProceed cannotProceed) {
            cannotProceed.printStackTrace();
        } catch (org.omg.CosNaming.NamingContextPackage.InvalidName invalidName) {
            invalidName.printStackTrace();
        }
        return objRef;
    }
}
