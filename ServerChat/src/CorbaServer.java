import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.CORBA.Object;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

public class CorbaServer {

    public static void main (String args[]) {

        ORB orb = ORB.init(args, null);
        Object objPoa, obj = null;
        POA rootPOA = null;
        try {
            objPoa = orb.resolve_initial_references("RootPOA");
            rootPOA = POAHelper.narrow(objPoa);
            obj = orb.resolve_initial_references("NameService");
        } catch (InvalidName invalidName) {
            invalidName.printStackTrace();
        }
        NamingContext naming = NamingContextHelper.narrow(obj);
        ChatManagerImpl chatManager = new ChatManagerImpl(args);
        Object objRef = null;
        try {
            objRef = rootPOA.servant_to_reference(chatManager);
        } catch (ServantNotActive servantNotActive) {
            servantNotActive.printStackTrace();
        } catch (WrongPolicy wrongPolicy) {
            wrongPolicy.printStackTrace();
        }
        NameComponent[] name = { new NameComponent("ChatManager", "") };
        try {
            naming.rebind(name, objRef);
        } catch (NotFound notFound) {
            notFound.printStackTrace();
        } catch (CannotProceed cannotProceed) {
            cannotProceed.printStackTrace();
        } catch (org.omg.CosNaming.NamingContextPackage.InvalidName invalidName) {
            invalidName.printStackTrace();
        }
        try {
            rootPOA.the_POAManager().activate();
        } catch (AdapterInactive adapterInactive) {
            adapterInactive.printStackTrace();
        }
        System.out.println("Server is ready ...");
        orb.run();
    }
}