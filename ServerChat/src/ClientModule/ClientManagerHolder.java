package ClientModule;

/**
* ClientModule/ClientManagerHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from IClientManager.idl
* Terça-feira, 19 de Junho de 2018 22h22min06s BRT
*/

public final class ClientManagerHolder implements org.omg.CORBA.portable.Streamable
{
  public ClientModule.ClientManager value = null;

  public ClientManagerHolder ()
  {
  }

  public ClientManagerHolder (ClientModule.ClientManager initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = ClientModule.ClientManagerHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    ClientModule.ClientManagerHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return ClientModule.ClientManagerHelper.type ();
  }

}
