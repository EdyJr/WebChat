public class MessageTuple {

    String sender, content;

    public MessageTuple(String sender, String content) {

        this.sender = sender;
        this.content = content;
    }
}
