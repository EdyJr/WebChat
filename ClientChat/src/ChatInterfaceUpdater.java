public interface ChatInterfaceUpdater {

    void displayMessage(String sender, String message);

    void saveMessage(String contactOfList, String name, String message);

    void addNewContact(String contactName);

    void updateStatus(String contact, boolean isOnline);

    boolean findContact(String contactName);
}
