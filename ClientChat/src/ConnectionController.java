import ChatModule.NonexistentContact;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CORBA.Object;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.CosNaming.NamingContextPackage.AlreadyBound;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import java.io.IOException;

public class ConnectionController {

    @FXML TextField nameField;
    @FXML TextField portField;
    @FXML TextField ipField;

    @FXML Label errorNameLabel;
    @FXML Label errorPortLabel;
    @FXML Label errorIPLabel;

    private ORB orb;
    private ClientManagerImpl clientManager;
    private NamingContext naming;
    POA rootPOA;

    public void initUI() {

        errorNameLabel.setVisible(false);
        errorPortLabel.setVisible(false);
        errorIPLabel.setVisible(false);
    }

    @FXML
    public void fieldsAction (KeyEvent event) {

        if (event.getCode() == KeyCode.ENTER)
            touchLoginButton();
    }

    @FXML
    public void touchLoginButton() {

        if (nameField.getText().isEmpty()) {
            errorNameLabel.setText("This field can't be empty");
            errorNameLabel.setVisible(true);
            errorPortLabel.setVisible(false);
            errorIPLabel.setVisible(false);
            return;
        }
        if (portField.getText().isEmpty())
            portField.setText("1050");
        if (ipField.getText().isEmpty())
            ipField.setText("localhost");

        final String name = nameField.getText();
        final String port = portField.getText();
        final String ip = ipField.getText();

        String args[] = new String[4];
        args[0] = "-ORBInitialPort";
        args[1] = port;
        args[2] = "-ORBInitialHost";
        args[3] = ip;

        try {
            initOrb(args, name);
            errorNameLabel.setVisible(false);
            errorPortLabel.setVisible(false);
            errorIPLabel.setVisible(false);
            login(args);
        } catch (InvalidName in){
            errorNameLabel.setText("Invalid Name");
            errorNameLabel.setVisible(true);
            errorPortLabel.setVisible(false);
            errorIPLabel.setVisible(false);
        }
    }

    private void nameError() {

        errorNameLabel.setText("Name already registered");
        errorNameLabel.setVisible(true);
        errorPortLabel.setVisible(false);
        errorIPLabel.setVisible(false);
    }

    private void connectionError() {

        errorNameLabel.setText("Check your connection");
        errorNameLabel.setVisible(true);
        errorPortLabel.setText("Check your connection");
        errorPortLabel.setVisible(true);
        errorIPLabel.setText("Check your connection");
        errorIPLabel.setVisible(true);
    }

    private void initOrb(String args[], String clientName) throws InvalidName {

        orb = ORB.init(args, null);
        Object objPoa = orb.resolve_initial_references("RootPOA");
        rootPOA = POAHelper.narrow(objPoa);
        Object obj = orb.resolve_initial_references("NameService");
        naming = NamingContextHelper.narrow(obj);
        clientManager = new ClientManagerImpl(clientName);
    }

    private void login(String args[]) {

        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/FXMLs/WebChatClient.fxml"));
        AnchorPane root;
        try {
            root = loader.load();
        } catch (IOException e) {
            System.out.println("Can't find fxml");
            return;
        }
        InterfaceController controller = loader.getController();
        try {
            controller.setupChatManager(args);
            if (!controller.setClient(clientManager)) {
                nameError();
                return;
            }
            controller.initUI();
            controller.initListener();
        } catch(Exception e) {
            connectionError();
            e.printStackTrace();
            return;
        }
        clientManager.setUpdater(controller);
        try {
            Object objRef = rootPOA.servant_to_reference(clientManager);
            String id = "ClientManager: " + clientManager.name;
            NameComponent[] name = {new NameComponent(id, "")};
            naming.rebind(name, objRef);
            rootPOA.the_POAManager().activate();
        } catch (Exception e) {
            connectionError();
            e.printStackTrace();
            return;
        }
        ipField.getScene().getWindow().hide();
        stage.setTitle("WebChat");
        stage.setScene(new Scene(root, 600, 500));
        stage.show();
        stage.setResizable(false);
        stage.setOnCloseRequest(event -> {
            controller.closeWindow();
            orb.shutdown(false);
            orb.destroy();
        });
        System.out.println("Client is ready ...");

        if (orb != null)
            new Thread(() -> orb.run()).start();
        else
            System.out.println("Null Orb");
    }
}
