import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class ClientServer extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/FXMLs/WebClientConnection.fxml"));
        AnchorPane root = loader.load();
        ConnectionController controller = loader.getController();
        controller.initUI();

        primaryStage.setTitle("WebChat Connection");
        primaryStage.setScene(new Scene(root, 300, 350));
        primaryStage.show();
        primaryStage.setResizable(false);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
