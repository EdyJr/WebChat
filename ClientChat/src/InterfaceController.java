import ChatModule.ChatManager;
import ChatModule.ChatManagerHelper;
import ChatModule.NonexistentContact;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import org.omg.CORBA.COMM_FAILURE;
import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CORBA.Object;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import queuecreator.QueueCreatorImplService;
import queuecreator.QueueCreatorItf;
import java.util.ArrayList;
import java.util.Hashtable;

public class InterfaceController implements ChatInterfaceUpdater {

    @FXML Label greetingsLabel;
    @FXML Label statusLabel;
    @FXML Label contactLabel;
    @FXML Label contactStatusLabel;

    @FXML Button loginButton;
    @FXML Button addButton;
    @FXML Button sendButton;

    @FXML TextField searchField;
    @FXML TextField messageField;

    @FXML TableView<TableElement> contactsTable;

    @FXML TableColumn nameColumn;
    @FXML TableColumn unseenColumn;

    @FXML TextArea chatArea;

    @FXML VBox chatBox;

    private final String notFound = "Contact not found";
    private final Color greenColor = Color.web("#39AEA8");
    private final Color redColor = Color.web("#EE220C");

    private ClientManagerImpl client;

    private ObservableList<TableElement> contactsList = FXCollections.observableArrayList();

    private TableElement selectedContact;

    private ChatManager chatManager;

    private Hashtable<String, ArrayList<MessageTuple>> msgList = new Hashtable<>();

    public boolean setClient(ClientManagerImpl client) {

        this.client = client;
        try {
            chatManager.getIsOnline(client.name); //Test if the name is already being used
        } catch (NonexistentContact nonexistentContact) {
            return true; //Name is not registered
        } catch (COMM_FAILURE cf) {
            return true; //Name is not registered
        }
        return false; //Name is already registered
    }

    public void setupChatManager(String args[]) throws InvalidName, CannotProceed,
            org.omg.CosNaming.NamingContextPackage.InvalidName, NotFound {

        ORB orb = ORB.init(args, null);
        Object obj = orb.resolve_initial_references("NameService");
        NamingContext chatContext = NamingContextHelper.narrow(obj);
        NameComponent[] name = { new NameComponent("ChatManager","") };
        Object objRef = chatContext.resolve(name);
        chatManager = ChatManagerHelper.narrow(objRef);
    }

    public void initUI() {

        chatBox.setVisible(false);
        String greetings = "Hi, ";
        greetingsLabel.setText(greetings + client.name);

        contactsTable.setItems(contactsList);
        contactsTable.setPlaceholder(new Label("No contacts added"));
        nameColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<TableElement, String>,
                ObservableValue<String>>) c -> c.getValue().getName());
        unseenColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<TableElement, String>,
                ObservableValue<String>>) c -> c.getValue().getUnseen());
        contactsTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                selectedContact = newValue;
                updateContact();
                for (TableElement ele : contactsList)
                    if (ele.getName().getValue().compareTo(selectedContact.getName().getValue()) == 0) {
                        ele.setUnseen("");
                        break;
                    }
                chatArea.setScrollTop(Double.MAX_VALUE);
            }
        });
        chatArea.textProperty().addListener(observable -> chatArea.setScrollTop(Double.MAX_VALUE));
        statusLabel.setTextFill(greenColor);
    }

    public void initListener() {

        QueueCreatorImplService service = new QueueCreatorImplService();
        QueueCreatorItf creatorServive = service.getQueueCreatorImplPort();
        creatorServive.createQueue(client.name);
    }

    private void incrementUnseenOf(String contactOfList) {

        for (TableElement ele : contactsList)
            if (ele.getName().getValue().compareTo(contactOfList) == 0) {
                String unseen = ele.getUnseen().getValue();
                int curCount = unseen.isEmpty() ? 0 : Integer.parseInt(unseen);
                ele.setUnseen(String.valueOf(curCount + 1));
                break;
            }
    }

    private void updateContact() {

        if (selectedContact == null)
            return;
        String contact = selectedContact.getName().getValue();
        try {
            boolean isContactOnline = chatManager.getIsOnline(contact);
            contactStatusLabel.setText(isContactOnline ? "Online" : "Offline");
            contactStatusLabel.setTextFill(isContactOnline ? greenColor : redColor);
        } catch (Exception e) {
            deleteContact(contact);
            return;
        }
        chatBox.setVisible(true);
        contactLabel.setText(contact);
        chatArea.clear();
        ArrayList<MessageTuple> list = msgList.get(contact);
        if (list == null)
            return;
        for (MessageTuple msg : list)
            displayMessage(msg.sender, msg.content);
    }

    public void closeWindow() {

        QueueCreatorImplService service = new QueueCreatorImplService();
        QueueCreatorItf creatorServive = service.getQueueCreatorImplPort();
        creatorServive.removeQueue(client.name);
    }

    private void deleteContact(String contactName) {

        showWarning(notFound);
        TableElement mustDelete = null;
        for (TableElement ele : contactsList)
            if (ele.getName().getValue().compareTo(contactName) == 0) {
                mustDelete = ele;
                break;
            }
        contactsList.remove(mustDelete);
        msgList.remove(contactName);
        if (contactsList.isEmpty()) {
            chatBox.setVisible(false);
            selectedContact = null;
        }
    }

    public void showWarning(String message) {

        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Invalid selection");
            alert.setHeaderText(null);
            alert.setContentText(message);
            alert.showAndWait();
        });
    }

    //WebChatClient fxml

    @FXML
    public void searchFieldAction (KeyEvent event) {

        if (event.getCode() == KeyCode.ENTER)
            touchAddButton();
    }

    @FXML
    public void sendFieldAction (KeyEvent event) {

        if (event.getCode() == KeyCode.ENTER)
            touchSendButton();
    }

    @FXML
    public void touchLoginButton() {

        boolean isContactOnline = !client.getIsOnline();
        client.setOnline(isContactOnline);
        loginButton.setText(isContactOnline ? "Logoff" : "Login");
        statusLabel.setText(isContactOnline ? "You're Online" : "You're Offline");
        statusLabel.setTextFill(isContactOnline ? greenColor : redColor);
        if (!isContactOnline) {
            addButton.setDisable(true);
            sendButton.setDisable(true);
        } else {
            addButton.setDisable(false);
            sendButton.setDisable(false);
            QueueCreatorImplService service = new QueueCreatorImplService();
            QueueCreatorItf creatorService = service.getQueueCreatorImplPort();

            ArrayList<String> list = (ArrayList<String>) creatorService.getOfflineMessages(client.name);
            for (String str : list) {
                String senderStr = "sender: ";
                String contentStr = "content: ";
                String sender = str.substring(senderStr.length(), str.indexOf(contentStr));
                String content = str.substring(str.indexOf(contentStr) + contentStr.length());

                if (!msgList.containsKey(sender))
                    msgList.put(sender, new ArrayList<>());
                msgList.get(sender).add(new MessageTuple(sender, content));
                if (selectedContact == null ||
                    selectedContact.getName().getValue().compareTo(sender) != 0)
                        incrementUnseenOf(sender);
            }
            updateContact();
        }
        for (TableElement ele : contactsList) {
            try {
                chatManager.updateStatus(ele.getName().getValue(), client.name, isContactOnline);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void touchAddButton() {

        String newFriend = searchField.getText();
        if (newFriend.isEmpty())
            return;
        if (newFriend.compareTo(client.name) == 0) {
            showWarning("You can't add yourself");
            searchField.setText("");
            return;
        }
        if (!client.getIsOnline()) {
            showWarning("Login to add friends");
            return;
        }
        try {
            chatManager.getIsOnline(newFriend);
        } catch (Exception e) {
            showWarning(notFound);
            searchField.setText("");
            return;
        }
        if (findContact(newFriend)) {
            showWarning("Contact already added");
            return;
        }
        addNewContact(newFriend);
        try {
            chatManager.establishFriendship(newFriend, client.name);
        } catch (NonexistentContact nonexistentContact) {
            showWarning(notFound);
        }
        searchField.setText("");
    }

    @FXML
    public void touchSendButton() {

        if (messageField.getText().isEmpty() ||
            selectedContact == null)
            return;
        try {
            String msg = messageField.getText();
            chatManager.writeMessage(client.name, selectedContact.getName().getValue(), msg);
            saveMessage(selectedContact.getName().getValue(), client.name, msg);
            displayMessage(client.name, msg);
            updateContact();
        } catch (Exception e) {
            deleteContact(selectedContact.getName().getValue());
        }
    }

    //ChatInterfaceUpdater Interface

    @Override
    public void saveMessage(String contactOfList, String name, String message) {

        if (!msgList.containsKey(contactOfList))
            msgList.put(contactOfList, new ArrayList<>());
        if (!findContact(contactOfList))
            addNewContact(contactOfList);
        msgList.get(contactOfList).add(new MessageTuple(name, message));
        if (selectedContact == null ||
            selectedContact.getName().getValue().compareTo(contactOfList) != 0)
            incrementUnseenOf(contactOfList);
    }

    @Override
    public void addNewContact(String contactName) {

        TableElement newContact = new TableElement(contactName);
        contactsList.add(newContact);
    }

    @Override
    public void displayMessage(String sender, String message) {

        if (sender.compareTo(client.name) == 0)
            messageField.setText("");
        if (selectedContact == null ||
           (selectedContact.getName().getValue().compareTo(sender) != 0 &&
            sender.compareTo(client.name) != 0))
            return;
        String content = sender + ": " + message + "\n\n";
        chatArea.appendText(content);
    }

    @Override
    public void updateStatus(String contact, boolean isOnline) {

        if (selectedContact == null ||
            selectedContact.getName().getValue().compareTo(contact) != 0 ||
            !client.getIsOnline())
                return;
        Platform.runLater(() -> {
            contactStatusLabel.setText(isOnline ? "Online" : "Offline");
            contactStatusLabel.setTextFill(isOnline ? greenColor : redColor);
        });
    }

    @Override
    public boolean findContact(String contactName) {

        for (TableElement ele : contactsList)
            if (ele.getName().getValue().compareTo(contactName) == 0)
                return true;
        return false;
    }
}