import ClientModule.ClientManagerPOA;

public class ClientManagerImpl extends ClientManagerPOA {

    public String name;
    private boolean isOnline;
    private ChatInterfaceUpdater updater;

    public ClientManagerImpl(String name) {

        this.name = name;
        this.isOnline = true;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public void setUpdater(ChatInterfaceUpdater updater) {
        this.updater = updater;
    }

    @Override
    public boolean getIsOnline() {
        return isOnline;
    }

    @Override
    public boolean writeToChat(String sender, String message) {

        updater.saveMessage(sender, sender, message);
        updater.displayMessage(sender, message);
        return false;
    }

    @Override
    public void addNewContact(String contactName) {

        if (!updater.findContact(contactName))
            updater.addNewContact(contactName);
    }

    @Override
    public void updateStatus(String contact, boolean isOnline) {
        updater.updateStatus(contact, isOnline);
    }
}
