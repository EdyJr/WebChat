import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TableElement {

    private final SimpleStringProperty name, unseen;

    public TableElement(String name) {

        this.name = new SimpleStringProperty(name);
        this.unseen = new SimpleStringProperty("");
    }

    public StringProperty getName() {
        return name;
    }

    public StringProperty getUnseen() { return unseen; }

    public void setName(String name) {
        this.name.set(name);
    }

    public void setUnseen(String unseen) {
        this.unseen.set(unseen);
    }
}
